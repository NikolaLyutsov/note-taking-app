# Note taking app

The Note taking app is a single page application written on React using Typescript.

# Visual
<img src='./note-taking-app/src/img/Notes.png'>
<img src='./note-taking-app/src/img/NewNote.png'>
<img src='./note-taking-app/src/img/SingleTag.png'>
<img src='./note-taking-app/src/img/EditTags.png'>

# What's included

The project is wrote on Visual Studio Code and using:

- HTML
- CSS
- Bootstrap
- React
- Typescript
